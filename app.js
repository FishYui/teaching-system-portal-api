const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const auth = require('./Auth/authentication');
const app = express();
const config = require('./config');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors({
    origin: [config.portalUrl],
    credentials: true
}));
app.use(cookieParser());
app.use(auth);
app.use((err, req, res, next) => {
    if (err.name === 'UnauthorizedError') {
        res.status(err.status).json({
            'success': false,
            'message': `Token Auth Error, The Err: ${err.message$}`
        });
    }
});

app.use('/', require('./routers/portal'));
app.use('/course', require('./routers/course'));

const PORT = process.env.PORT || 6001;
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
})