const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const config = require('../config');
const fetch = require('../script/fetch');

const router = express.Router();

router.get('/auth', (req, res) => {
    res.json({ 'success': true, 'message': 'Auth OK', 'payload': req.user });
});

router.post('/login', async (req, res) => {
    try {
        const username = req.body.username;
        const password = req.body.password;

        if (username != undefined && password != undefined) {

            const response = await fetch(`${config.fhirServer}/Person?identifier=useFor|${config.useFor}&identifier=username|${username}`);

            if (response.total === 1) {

                const personData = response.entry[0].resource;
                const DBpassword = personData.identifier.filter(i => i.system === 'password')[0].value;

                if (bcrypt.compareSync(password, DBpassword)) {

                    const id = personData.link[0].target.reference.split('/')[1];
                    const name = personData.name[0].text;
                    const role = personData.link[0].target.display;
                    let url = '';
                    let courseId = [];

                    if (role != 'admin') {
                        switch (role) {
                            case 'student':
                                url = `${config.fhirServer}/Appointment?identifier=useFor|TCU-H707&patient=${id}&_elements=slot`;
                                break;
                            case 'teacher':
                                url = `${config.fhirServer}/Appointment?identifier=useFor|TCU-H707&practitioner=${id}&_elements=slot`;
                                break;
                        }

                        const responseCourses = await fetch(url);
                        courseId = responseCourses.entry.map(e => e.resource.slot[0].reference.split('/')[1]);
                    }

                    const token = 'Bearer ' + jwt.sign(
                        {
                            '_id': id,
                            'name': name,
                            'role': role,
                            'course': courseId
                        },
                        config.secret,
                        {
                            expiresIn: config.expiresIn
                        }
                    );

                    res.cookie('PortalToken', token, { httpOnly: true, maxAge: config.expiresIn * 1000 });
                    res.json({ 'success': true, 'message': 'Login Success!' });

                } else {
                    //密碼不一致
                    res.json({ 'success': false, 'message': 'PassWord Error!' });
                }
            } else if (response.total === 0) {
                //找不到帳號
                res.json({ 'success': false, 'message': 'No This Account!' });
            } else {
                //找到過多帳號，系統有漏洞
                res.json({ 'success': false, 'message': 'Found Too More Account!' });
            }
        } else {
            //未完全輸入
            res.json({ 'success': false, 'message': 'Input Error!' });
        }
    } catch (err) {
        //與FHIR Server連線錯誤
        res.json({ 'success': false, 'status': err.response.status, 'message': err.response.statusText });
    }
});

router.get('/logout', (req, res) => {
    res.clearCookie('PortalToken');
    res.json({ 'success': true, 'message': 'LogOut Success!' });
});

module.exports = router;