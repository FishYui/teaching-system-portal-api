const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config');
const fetch = require('../script/fetch');

const router = express.Router();

router.get('/resources', async (req, res) => {
    try {
        const peopleId = req.user._id;
        const role = req.user.role;
        let url = '';
        switch (role) {
            case 'student':
                url = `${config.fhirServer}/Appointment?identifier=useFor|TCU-H707&patient=${peopleId}&_elements=slot`;
                break;
            case 'teacher':
                url = `${config.fhirServer}/Appointment?identifier=useFor|TCU-H707&practitioner=${peopleId}&_elements=slot`;
                break;
            case 'admin':
                url = `${config.fhirServer}/Appointment?identifier=useFor|TCU-H707&_elements=slot`;
                break;
        }
        const response = await fetch(url);
        res.json({ 'success': true, 'data': response });
    } catch (err) {
        res.json({ 'success': false, 'status': err.response.status, 'message': err.response.statusText });
    }
});

//檢查調閱的教材是否與token內允許調閱的教材一致
router.get(/\/documentReference(s)?\/*/, (req, res, next) => {
    const role = req.user.role;
    const course = req.user.course;
    const slotId = req.query.slotId;
    
    if (role === 'admin') {
        next();
    } else {
        const checkSlot = course.find((item) => slotId === item);

        if (checkSlot != undefined) {
            next();
        } else {
            res.json({ 'success': true, 'message': 'You don\'t have the Authority!' });
        }
    }
});

//調閱教材
router.get('/documentReferences', async (req, res) => {
    const slotId = req.query.slotId;

    try {
        const response = {
            'slot': await fetch(`${config.fhirServer}/Slot/${slotId}`),
            'documentReference': await fetch(`${config.fhirServer}/DocumentReference?identifier=useFor|TCU-H707&identifier=slotId|${slotId}`)
        };

        res.json({ 'success': true, 'data': response });
    } catch (err) {
        res.json({ 'success': false, 'status': err.response.status, 'message': err.response.statusText });
    }
});

//取得下載教材的授權token
router.get('/documentReference/:documentReferenceId', async (req, res) => {

    const documentReferenceId = req.params.documentReferenceId;
    const slotId = req.query.slotId;
    const attachmentUrl = req.query.attachmentUrl;

    try {
        //驗證要調閱的教材是否存在並且在合法的課程內
        const checkDocumentReference = await fetch(encodeURI(`${config.fhirServer}/DocumentReference?_id=${documentReferenceId}&location=${attachmentUrl}&identifier=slotId|${slotId}`));
        if (checkDocumentReference.total === 1) {

            const token = 'Bearer ' + jwt.sign(
                {
                    'iss': config.selfUrl,
                    'aud': config.materialUrl,
                    'scope': {
                        'method': 'GET',
                        'url': attachmentUrl
                    }
                },
                config.secret,
                {
                    expiresIn: 2
                }
            );

            res.json({ 'success': true, 'token': token });
        } else {
            res.json({ 'success': true, 'message': 'You don\'t have the Authority!' });
        }
    } catch (err) {
        res.json({ 'success': false, 'status': err.response.status, 'message': err.response.statusText });
    }
});

module.exports = router;