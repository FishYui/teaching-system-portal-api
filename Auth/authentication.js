const expressJwt = require('express-jwt');
const config = require('../config');

const authentication = expressJwt({
    secret: config.secret,
    getToken: (req) => {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer')
            return req.headers.authorization.split(' ')[1];
        else if (req.cookies.PortalToken && req.cookies.PortalToken.split(' ')[0] === 'Bearer')
            return req.cookies.PortalToken.split(' ')[1];
        else
            return null;
    }
}).unless({
    path: [
        '/login', '/logout'
    ]
});

module.exports = authentication;