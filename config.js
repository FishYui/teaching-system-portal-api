const config = {
    'secret': 'e802107e3c8a2fe5658b88cef4ec429f',
    'useFor': 'TCU-H707',
    'expiresIn': 60 * 15,
    //'fhirServer': 'https://hapi.fhir.tw/fhir',
    'fhirServer': 'http://203.64.84.213:8080/hapi-fhir-jpaserver/fhir',

    //local
    'selfUrl': 'http://localhost:6001',
    'materialUrl': 'http://localhost:6002',
    'portalUrl': 'http://localhost:5500',

    //server
    // 'selfUrl': 'http://203.64.84.213:6001',
    // 'materialUrl': 'http://203.64.84.213:6002',
    // 'portalUrl': 'http://203.64.84.213:5500',
}

module.exports = config;