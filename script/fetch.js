const fetch = require('node-fetch'); 

module.exports = async (url, options) => {
    const opt = options || {};

    const response = await fetch(url, opt);
    if (response.status >= 200 && response.status < 300) {
        return await response.json();
    } else {
        const error = new Error(response.statusText);
        error.response = response;
        throw error;
    }
}